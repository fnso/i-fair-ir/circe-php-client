<?php
declare(strict_types=1);

namespace Certic\Circe;

class LogEntry
{
    const LEVEL_DEBUG = 0;
    const LEVEL_INFO = 1;
    const LEVEL_WARNING = 2;
    const LEVEL_ERROR = 3;
    const LEVEL_FATAL = 4;
    private array $log_dict;

    public function __construct(array $log_dict)
    {
        $this->log_dict = $log_dict;
    }

    public function getTimestamp(): ?\DateTime
    {
        if (array_key_exists('timestamp', $this->log_dict)) {
            try {
                // stamp should be in "%Y-%m-%dT%H:%M:%S.%fZ" form.
                $stamp = new \DateTime($this->log_dict['timestamp']);
                return $stamp;
            } catch (\Exception $e) {
                // parse date may fail
                error_log($e->getMessage());
            }
        }
        return null;
    }

    public function getMessage(): ?string
    {
        if (array_key_exists('message', $this->log_dict)) {
            return (string)$this->log_dict['message'];
        }
        return null;
    }

    public function getLevel(): ?int
    {
        if (array_key_exists('level', $this->log_dict)) {
            switch ($this->log_dict['level']) {
                case 'DEBUG':
                    return self::LEVEL_DEBUG;
                case 'WARNING':
                    return self::LEVEL_WARNING;
                case 'ERROR':
                    return self::LEVEL_ERROR;
                case 'FATAL':
                case 'CRITICAL':
                    return self::LEVEL_FATAL;
                default:
                    return self::LEVEL_INFO;
            }
        }
        return null;
    }

    public function getLevelLabel(): ?string
    {
        if (array_key_exists('level', $this->log_dict)) {
            switch ($this->log_dict['level']) {
                case 'DEBUG':
                    return 'DEBUG';
                case 'WARNING':
                    return 'WARNING';
                case 'ERROR':
                    return 'ERROR';
                case 'FATAL':
                case 'CRITICAL':
                    return 'FATAL';
                default:
                    return 'INFO';
            }
        }
        return null;
    }
}