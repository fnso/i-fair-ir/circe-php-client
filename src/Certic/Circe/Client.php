<?php
declare(strict_types=1);

namespace Certic\Circe;

class Client
{
    private $endpoint;
    private $uuid;
    private $secret;

    public function __construct(string $api_endpoint = null, string $secret_key = null, string $app_uuid = null)
    {
        $this->endpoint = $api_endpoint ?: getenv('CIRCE_ENDPOINT');
        $this->secret = $secret_key ?: getenv('CIRCE_SECRET');
        $this->uuid = $app_uuid ?: getenv('CIRCE_APP_UUID');
        if (!$this->endpoint || !$this->secret || !$this->uuid) {
            throw new Exception("Credentials and/or endpoint not set");
        }
    }

    public function availableTransformations(): array
    {
        $endpoint = $this->endpoint . '/transformations/';
        $endpoint = \str_replace('//transformations/', '/transformations/', $endpoint);
        $body = file_get_contents($endpoint);
        if ($body === false) {
            throw new Exception('Could not fetch available transformations.');
        }
        $data = json_decode($body, true);
        if (is_null($data)) {
            throw new Exception('Could not fetch available transformations.');
        }
        return $data;
    }

    public function send(Job $job, bool $wait = false, string $destination_file = null)
    {
        if (!$destination_file) {
            $destination_file = \tempnam(\sys_get_temp_dir(), "circe") . '.tar.gz';
        }
        $job_archive_path = $job->makeArchive();
        $job_hash = \hash_hmac_file("sha256", $job_archive_path, $this->secret);
        $endpoint = $wait ? $this->endpoint . '/job/?block=1' : $this->endpoint . "/job/";
        $endpoint = \str_replace('//job', '/job', $endpoint);
        $ch = \curl_init();
        \curl_setopt($ch, CURLOPT_URL, $endpoint);
        \curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        \curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        \curl_setopt($ch, CURLOPT_POST, true);
        \curl_setopt($ch, CURLOPT_POSTFIELDS, \file_get_contents($job_archive_path));
        \curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type:application/json',
            'Content-Length: ' . \filesize($job_archive_path),
            'Authorization:' . $this->uuid . ' ' . $job_hash,
        ]);
        $output = \curl_exec($ch);
        if ($output === false) {
            throw new Exception(\curl_error($ch));
        }
        $this->checkStatusCode($ch);
        if ($wait) {
            \file_put_contents($destination_file, $output);
            $job->setResultFilePath($destination_file);
        } else {
            $job->setUuid($output);
        }
    }


    public function fetch(Job $job, string $destination_file = null)
    {
        if (!$job->getUuid()) {
            throw new Exception('Job has to be first sent to Circe server.');
        }
        if (!$destination_file) {
            $destination_file = \tempnam(\sys_get_temp_dir(), "circe") . '.tar.gz';
        }
        $endpoint = $this->endpoint . '/job/' . $job->getUuid();
        $endpoint = \str_replace('//job', '/job', $endpoint);
        $job_hash = \hash_hmac("sha256", $job->getUuid(), $this->secret);
        $ch = \curl_init();
        \curl_setopt($ch, CURLOPT_URL, $endpoint);
        \curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        \curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization:' . $this->uuid . ' ' . $job_hash,
        ]);
        $output = \curl_exec($ch);
        if ($output === false) {
            throw new Exception(\curl_error($ch));
        }
        $this->checkStatusCode($ch);
        \file_put_contents($destination_file, $output);
        $job->setResultFilePath($destination_file);
    }

    public function newJob(): Job
    {
        return new Job();
    }

    private function checkStatusCode($curl_handle)
    {
        $infos = curl_getinfo($curl_handle);
        if ($infos['http_code'] !== 200) {
            if ($infos['http_code'] === 202) {
                throw new Exception('Job exists but result is not ready yet, try again later.');
            }
            if ($infos['http_code'] === 404) {
                throw new Exception('No such job on the server.');
            }
            if ($infos['http_code'] === 403) {
                throw new Exception('Forbidden. Check your credentials.');
            }
        }
    }
}
