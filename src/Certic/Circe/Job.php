<?php
declare(strict_types=1);

namespace Certic\Circe;

class Job
{
    private $files = [];
    private $transformations = [];
    private $hook;
    private $dir_path;
    private $result_file_path;
    private $uuid;


    public function __construct(string $uuid = null)
    {
        $this->uuid = $uuid;
    }

    public function addFile(string $file_path): Job
    {
        $this->files[] = $file_path;
        return $this;
    }

    public function addTransformation(string $name, array $options = null): Job
    {
        if ($options) {
            $this->transformations[] = ["name" => $name, "options" => $options];
        } else {
            $this->transformations[] = ["name" => $name];
        }
        return $this;
    }

    public function setNotifyHook(string $web_hook): Job
    {
        $this->hook = $web_hook;
        return $this;
    }

    public function makeArchive(): string
    {
        if ($this->dir_path) {
            $this->deleteTree($this->dir_path);
        }
        $this->dir_path = sys_get_temp_dir() . DIRECTORY_SEPARATOR . uniqid() . DIRECTORY_SEPARATOR;
        if (!mkdir($this->dir_path, 0777, true)) {
            throw new Exception("Could not create temporary directory.");
        }
        $tar_path = $this->dir_path . "job.tar";
        $tar = new \PharData($tar_path);
        if ($this->hook) {
            $tar->addFromString("job.json", json_encode([
                "transformations" => $this->transformations,
                "notify_hook" => $this->hook
            ]));
        } else {
            $tar->addFromString("job.json", json_encode([
                "transformations" => $this->transformations
            ]));
        }


        foreach ($this->files as $file) {
            $tar->addFile($file, \basename($file));
        }

        $tar->compress(\Phar::GZ);
        return $tar_path;
    }

    public function __destruct()
    {
        if ($this->dir_path) {
            $this->deleteTree($this->dir_path);
        }
    }

    private function deleteTree(string $folder, bool $keepRootFolder = false): bool
    {
        if (empty($folder) || !\file_exists($folder)) {
            return true;
        } elseif (\is_file($folder) || \is_link($folder)) {
            return @\unlink($folder);
        }

        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($folder, \RecursiveDirectoryIterator::SKIP_DOTS),
            \RecursiveIteratorIterator::CHILD_FIRST
        );

        foreach ($files as $fileinfo) {
            $action = ($fileinfo->isDir() ? 'rmdir' : 'unlink');
            if (!@$action($fileinfo->getRealPath())) {
                return false; // Abort due to the failure.
            }
        }

        return (!$keepRootFolder ? @rmdir($folder) : true);
    }

    public function getResultFilePath(): string
    {
        if (!$this->result_file_path) {
            throw new Exception('Fetch job result file from Circe server first.');
        }
        return $this->result_file_path;
    }

    public function setResultFilePath(string $result_file_path): Job
    {
        $this->result_file_path = $result_file_path;
        return $this;
    }

    public function setUuid(string $uuid):Job
    {
        $this->uuid = $uuid;
        return $this;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getResult(): JobResult
    {
        return new JobResult($this->getResultFilePath());
    }
}
