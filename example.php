<?php
include __DIR__ . '/src/Certic/Circe/Exception.php';
include __DIR__ . '/src/Certic/Circe/LogEntry.php';
include __DIR__ . '/src/Certic/Circe/JobResult.php';
include __DIR__ . '/src/Certic/Circe/Job.php';
include __DIR__ . '/src/Certic/Circe/Client.php';

use \Certic\Circe\Client as Client;

$client = new Client("http://localhost:8000/", "not a key", "not a uuid");

/*
 * Retourne les détails sur les transformations disponibles sur le serveur.
 * Exemple, une transformation s'appellant "doc2tei", ayant une option
 * "keep_transient_files" qui accepte une valeur "oui" ou "non", qui est à
 * "non" par défaut.
 *
 * {
 *   "doc2tei": {
 *       "label": "Docx vers TEI",
 *       "help": "Convertir les fichiers *.docx et *.odt en fichiers *.xml (vocabulaire TEI)",
 *       "options": [
 *           {
 *               "id": "keep_transient_files",
 *               "label": "garder les fichiers interm\u00e9diaires",
 *               "values": {
 *                   "oui": "oui",
 *                   "non": "non"
 *               },
 *               "default": "non",
 *               "free_input": false
 *           }
 *       ]
 *   }
 * }
 *
 */
var_dump($client->availableTransformations());

/**
 * On commence par créer un nouveau job vide
 */
$job = $client->newJob();

/**
 * Puis on ajoute des fichiers à traiter à ce job
 * en fournissant le chemin vers le fichier;
 */
$job->addFile('/chemin/vers/fichier.docx');


/**
 * On ajoute ensuite les transformations souhaitées avec
 * leurs options éventuelles.
 * Il est possible de chainer des transformations,
 * il suffit d'appeler successivement addTransformation()
 */
$job->addTransformation('doc2tei', ['keep_transient_files' => 'non']);

/**
 * On envoit ensuite le job au serveur.
 * Avec wait=true on indique qu'on souhaite attendre
 * la fin du job et obtenir le résultat dans la réponse
 * HTTP.
 */
$client->send($job, true); // synchronous call

/**
 * Le résultat de la transformation est alors
 * disponible:
 */
$result = $job->getResult();

/**
 * On peut éventuellement lire le fichier de log
 * propre au résultat du job afin de détecter les
 * erreurs et les avertissements.
 */
foreach ($result->getLog() as $entry) {
    echo $entry->getTimeStamp()->format('Y-m-d H:i:s')
        . ' [' . $entry->getLevelLabel() . ']: '
        . $entry->getMessage() . PHP_EOL;
}

/**
 * On peut finalement récupérer les documents
 * transformés:
 */
foreach ($result->getDocumentsPaths() as $file_path) {
    echo file_get_contents($file_path) . PHP_EOL;
}