# Circe PHP client lib

## Dependencies

PHP 7 with curl and json extensions.

## Usage

    <?php
    declare(strict_types=1);
    include "vendor/autoload.php";
    use Certic\Circe\Client as CirceClient;

    $client = new CirceClient(
        'http://acme.tld',
        'dont_tell_anyone',
        'app_id');
    $job = $client->newJob();
    $job->addTransformation("html2pdf");
    $job->addFile("/home/someone/example.html");
    $client->send($job, true); // wait=true for synchronous execution

    foreach ($job->getResult()->getFiles() as $file){
        if($file->getFileName() == "out.log"){
            echo file_get_contents((string)$file).PHP_EOL;
        }else{
            echo $file.PHP_EOL;
        }
    }