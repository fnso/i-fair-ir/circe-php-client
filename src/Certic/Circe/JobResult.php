<?php
declare(strict_types=1);

namespace Certic\Circe;

class JobResult
{
    private $result_file_path;
    public function __construct(string $result_file_path)
    {
        $this->result_file_path = $result_file_path;
    }

    /**
     * Returns an Iterator over files contained
     * in the returned archive.
     *
     * @return \Iterator
     */
    public function getFiles(): \Iterator
    {
        $tar = new \PharData($this->result_file_path);
        foreach ($tar as $file) {
            yield $file;
        }
    }

    /**
     * Return an Iterator over the documents paths,
     * excluding the jobs definition file (job.json)
     * and the log file (out.log).
     *
     * Paths are of type 'phar:///path/to/file.ext' and
     * can be opened with any PHP builtin function
     * compatible with stream wrappers.
     *
     * @return string[]
     */
    public function getDocumentsPaths(): \Iterator
    {
        foreach($this->getFiles() as $file) {
            if (!in_array($file->getFileName(), ['out.log', 'job.json'])) {
                yield (string)$file;
            }
        }
    }

    /**
     * @return LogEntry[]
     */
    public function getLog(): \Iterator
    {
        foreach ($this->getFiles() as $file){
            if($file->getFileName() == 'out.log'){
                $content = file_get_contents((string)$file);
                $lines = explode(PHP_EOL, $content);
                foreach ($lines as $line){
                    $entry = json_decode($line, true);
                    if($entry){
                        yield new LogEntry($entry);
                    }
                }
            }
        }
    }

    public function __destruct()
    {
        @\unlink($this->result_file_path);
    }
}
